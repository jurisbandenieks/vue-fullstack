import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateEmployeeTable1550411910684 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE "public"."employee" (
        "id" integer DEFAULT nextval('employee_id_seq') NOT NULL,
        "firstname" character varying(500) NOT NULL,
        "lastname" character varying(500) NOT NULL,
        "description" text NOT NULL,
        CONSTRAINT "PK_3c2bc72f03fd5abbbc5ac169498" PRIMARY KEY ("id")
      ) WITH (oids = false)
    `)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
