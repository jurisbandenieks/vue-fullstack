import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreatePhotoSeq1546172777739 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE SEQUENCE photo_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1
    `)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      DROP SEQUENCE IF EXISTS photo_id_seq
    `)
  }
}
