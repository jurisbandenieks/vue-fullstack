import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreatePhotoTable1546172816816 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE "photo" (
        "id" integer DEFAULT nextval('photo_id_seq') NOT NULL,
        "name" character varying(500) NOT NULL,
        "description" text NOT NULL,
        "filename" character varying NOT NULL,
        "views" integer NOT NULL,
        "isPublished" boolean NOT NULL,
        CONSTRAINT "PK_723fa50bf70dcfd06fb5a44d4ff" PRIMARY KEY ("id")
      ) WITH (oids = false)
    `)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      DROP TABLE IF EXISTS "photo"
    `)
  }
}
