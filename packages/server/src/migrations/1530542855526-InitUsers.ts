import { MigrationInterface, QueryRunner } from 'typeorm'

export class InitUsers1530542855526 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      INSERT INTO "user" ("fullName", "username", "email", "password", "role")
      VALUES ('Test User', 'username', 'user@network.com', '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG', 'User')
    `)
    await queryRunner.query(`
      INSERT INTO "user" ("fullName", "username", "email", "password", "role")
      VALUES ('Test Admin', 'administrator', 'admin@network.com', '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG', 'Admin')
    `)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
