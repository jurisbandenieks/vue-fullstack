import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateTableUsers1530542855525 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE "user" (
        "id" integer DEFAULT nextval('user_id_seq') NOT NULL,
        "created" timestamp DEFAULT now() NOT NULL,
        "fullName" text NOT NULL,
        "role" character varying DEFAULT 'User' NOT NULL,
        "email" text NOT NULL,
        "username" text NOT NULL,
        "password" text NOT NULL,
        CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"),
        CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"),
        CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email")
      ) WITH (oids = false)
    `)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      DROP TABLE IF EXISTS "user"
    `)
  }
}
