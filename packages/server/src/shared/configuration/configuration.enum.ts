export enum Configuration {
  API_PORT = 'API_PORT',
  API_HOST = 'API_HOST',
  SECRET = 'SECRET'
}
