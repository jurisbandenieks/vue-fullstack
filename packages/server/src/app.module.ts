import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core'
import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { HttpErrorFilter } from './shared/HttpError.filter'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { PhotoModule } from './photo/photo.module'
import { LoggingInterceptor } from './shared/Logging.interceptor'
import { UserModule } from './user/user.module'
import { SharedModule } from './shared/shared.module'
import { ConfigurationService } from './shared/configuration/configuration.service'
import { Configuration } from './shared/configuration/configuration.enum'
import { EmployeeModule } from './employee/employee.module'

@Module({
  imports: [
    TypeOrmModule.forRoot(ConfigurationService.dbConfig),
    PhotoModule,
    SharedModule,
    UserModule,
    EmployeeModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor
    }
  ]
})
export class AppModule {
  static host: string
  static port: number | string
  static isDev: boolean

  constructor(private readonly configrationService: ConfigurationService) {
    AppModule.port = AppModule.normalizePort(
      configrationService.get(Configuration.API_PORT)
    )
    AppModule.host = configrationService.get(Configuration.API_HOST)
    AppModule.isDev = configrationService.isDevelopment
  }

  private static normalizePort(portin: string | number): string | number {
    const portNumber: number =
      typeof portin === 'string' ? parseInt(portin, 10) : portin
    if (isNaN(portNumber)) {
      return portin
    } else {
      return portNumber
    }
  }
}
