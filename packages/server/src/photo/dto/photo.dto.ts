import { IsString, IsNumber, IsBoolean } from 'class-validator'
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger'

export class PhotoDTO {
  @ApiModelPropertyOptional()
  id: number

  @IsString()
  @ApiModelProperty()
  name: string

  @IsString()
  @ApiModelPropertyOptional()
  description: string

  @IsString()
  @ApiModelProperty()
  filename: string

  @IsNumber()
  @ApiModelPropertyOptional()
  views: number

  @IsBoolean()
  @ApiModelProperty()
  isPublished: boolean
}
