import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  UsePipes,
  Param,
  Body,
  UseGuards,
  Query
} from '@nestjs/common'
import { ApiBearerAuth } from '@nestjs/swagger'
import { AuthGuard } from '@nestjs/passport'

import { PhotoService } from './photo.service'
import { Photo } from './photo.entity'
import { PhotoDTO } from './dto/photo.dto'
import { ValidationPipe } from '../shared/Validation.pipe'
import { Roles } from '../shared/decorators/roles.decorator'
import { UserRole } from '../shared/UserRole'
import { RolesGuard } from '../shared/guards/roles.guard'

@Controller('/photo')
@ApiBearerAuth()
export class PhotoController {
  constructor(private readonly photoService: PhotoService) {}

  @Get()
  async index(@Query() query?): Promise<Photo[]> {
    return await this.photoService.index(query)
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Photo> {
    return await this.photoService.show(id)
  }

  @Post()
  @UsePipes(new ValidationPipe())
  @Roles(UserRole.user, UserRole.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async create(@Body() data: PhotoDTO): Promise<Photo> {
    return await this.photoService.create(data)
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  @Roles(UserRole.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async update(
    @Param('id') id: string,
    @Body() data: PhotoDTO
  ): Promise<Photo> {
    return await this.photoService.update(id, data)
  }

  @Delete(':id')
  @Roles(UserRole.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async delete(@Param('id') id: string) {
    return await this.photoService.destroy(id)
  }
}
