import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  UsePipes
} from '@nestjs/common'
import { UserService } from './user.service'
import { UserLoginDTO } from './dto/UserLogin.dto'
import { UserRegisterDTO } from './dto/UserRegister.dto'
import { LoginResponseDTO } from './dto/LoginResponse.dto'
import { plainToClass } from 'class-transformer'
import { UserDTO } from './dto/User.dto'

@Controller('/auth')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/local')
  @UsePipes(new ValidationPipe())
  async login(@Body() data: UserLoginDTO): Promise<LoginResponseDTO> {
    const user = await this.userService.login(data)
    return user
  }

  @Post('/local/register')
  @UsePipes(new ValidationPipe())
  async register(@Body() data: UserRegisterDTO) {
    return await this.userService.register(data)
  }
}
