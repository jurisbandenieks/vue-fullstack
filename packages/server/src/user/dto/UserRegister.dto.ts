import { IsNotEmpty, IsString } from 'class-validator'
import { UserLoginDTO } from './UserLogin.dto'
import { ApiModelProperty } from '@nestjs/swagger'

export class UserRegisterDTO extends UserLoginDTO {
  @IsNotEmpty()
  @IsString()
  @ApiModelProperty()
  fullName: string

  @IsNotEmpty()
  @IsString()
  @ApiModelProperty()
  email: string
}
