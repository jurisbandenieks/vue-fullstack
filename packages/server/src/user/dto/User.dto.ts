import { UserRole } from '../../shared/UserRole'
import { Exclude } from 'class-transformer'
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger'

export class UserDTO {
  @ApiModelProperty()
  fullName: string

  @ApiModelProperty()
  username: string

  @ApiModelPropertyOptional({ enum: UserRole })
  role?: UserRole

  @Exclude()
  @ApiModelProperty()
  password: string
}
