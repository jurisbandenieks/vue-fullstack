import { UserDTO } from './User.dto'

export class LoginResponseDTO {
  token: string
  user: UserDTO
}
