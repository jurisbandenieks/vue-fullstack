import { IsNotEmpty, IsString, Length } from 'class-validator'
import { ApiModelProperty } from '@nestjs/swagger'

export class UserLoginDTO {
  @IsNotEmpty()
  @IsString()
  @Length(6)
  @ApiModelProperty({ type: String })
  username: string

  @IsNotEmpty()
  @IsString()
  @Length(6)
  @ApiModelProperty({ type: String })
  password: string
}
