import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'
import { EmployeeService } from './employee.service'
import { Employee } from './employee.entity'

describe('PhotoService', () => {
  let service: EmployeeService
  const mockRepository = {
    find() {
      return [
        {
          id: 1,
          firstname: 'First',
          lastname: 'Firstlast',
          description: 'First Description'
        },
        {
          id: 2,
          firstname: 'Second',
          lastname: 'Secondlast',
          description: 'Second Description'
        }
      ]
    }
  }

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EmployeeService,
        {
          provide: getRepositoryToken(Employee),
          useValue: mockRepository
        }
      ]
    }).compile()
    service = module.get<EmployeeService>(EmployeeService)
  })
  it('should be defined', () => {
    expect(service).toBeDefined()
  })
  it('should return list of photos for index', async () => {
    const employees = await service.index()
    expect(employees).toEqual([
      {
        description: 'First Description',
        id: 1,
        firstname: 'First',
        lastname: 'Firstlast'
      },
      {
        description: 'Second Description',
        id: 2,
        firstname: 'Second',
        lastname: 'Secondlast'
      }
    ])
  })
})
