import { Test, TestingModule } from '@nestjs/testing'
import { EmployeeController } from './employee.controller'
import { EmployeeService } from './employee.service'

describe('Employee Controller', () => {
  let module: TestingModule
  let controller: EmployeeController

  const mockService = {
    index() {
      return []
    }
  }

  beforeAll(async () => {})

  it('should be defined', async () => {
    module = await Test.createTestingModule({
      controllers: [EmployeeController],
      providers: [
        {
          provide: EmployeeService,
          useValue: mockService
        }
      ]
    }).compile()
    controller = module.get<EmployeeController>(EmployeeController)
    expect(controller).toBeDefined()
  })
})
