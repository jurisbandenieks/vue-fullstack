import { IsString } from 'class-validator'
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger'

export class EmployeeDTO {
  @ApiModelPropertyOptional()
  id: number

  @IsString()
  @ApiModelProperty()
  firstname: string

  @IsString()
  @ApiModelProperty()
  lastname: string

  @IsString()
  @ApiModelPropertyOptional()
  description: string
}
