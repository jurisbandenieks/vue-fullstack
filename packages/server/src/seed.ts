import { createQueryBuilder } from 'typeorm'
import { User } from './user/user.entity'
import { Employee } from './employee/employee.entity'
import { UserRole } from './shared/UserRole'

export async function seed() {
  console.log('Filling database with test data...')
  const qry = createQueryBuilder()

  try {
    /* Cleanup */
    console.log('Clean up...')
    await qry
      .delete()
      .from(User)
      .execute()
    await qry
      .delete()
      .from(Employee)
      .execute()

    //#region /* Users */
    console.log('Users...')
    const user1 = new User()
    user1.username = 'username'
    user1.fullName = 'Test User'
    user1.email = 'user@network.com'
    // passwsss
    user1.password =
      '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG'
    await user1.save()
    const user2 = new User()
    user2.username = 'administrator'
    user2.fullName = 'Admin User'
    user2.email = 'admin@network.com'
    user2.role = UserRole.admin
    // passwsss
    user2.password =
      '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG'
    await user2.save()
    //#endregion

    //#region /* Employee */
    console.log('Employee...')
    let emp1 = new Employee()
    emp1.firstname = 'Firstname'
    emp1.lastname = 'Lastname'
    emp1.description = 'Your commpany first employoee'
    emp1 = await emp1.save()
    //#endregion
  } catch (error) {
    console.log('Internal error while seed.', error)
  }
}
