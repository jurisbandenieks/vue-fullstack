import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigurationService } from '../src/shared/configuration/configuration.service'

export const testDatabase = entities => {
  return TypeOrmModule.forRoot(ConfigurationService.dbConfig)
}
