import {
  Module,
  VuexModule,
  MutationAction,
  getModule
} from 'vuex-module-decorators'
import axios from 'axios'

import store from '@/store'
import { Photo } from './photos.model'

@Module({
  namespaced: true,
  name: 'photos',
  store,
  dynamic: true
})
class PhotosModule extends VuexModule {
  public photos: Photo[] = []

  @MutationAction
  public async getPhotos() {
    const { data } = await axios.get('/api/photo')
    return { photos: data as Photo[] }
  }
}

export default getModule(PhotosModule)
