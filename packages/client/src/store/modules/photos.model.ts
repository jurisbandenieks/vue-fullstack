export interface Photo {
  name: string
  description: string
  filename: string
  views: number
  isPublished: boolean
}
