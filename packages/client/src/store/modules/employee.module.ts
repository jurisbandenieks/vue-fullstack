import {
  Module,
  VuexModule,
  Action,
  getModule,
  Mutation
} from 'vuex-module-decorators'
import axios from 'axios'

import store from '@/store'
import { Employee, EmployeePayload } from './employee.model'

@Module({
  namespaced: true,
  name: 'employee',
  store,
  dynamic: true
})
class EmployeeModule extends VuexModule {
  public items: Employee[] = []
  public defaultItem: Employee = {
    firstname: '',
    lastname: '',
    description: ''
  }
  public editedItem: Employee = {
    id: 0,
    firstname: '',
    lastname: '',
    description: ''
  }
  public editedIndex = -1
  public loading = false
  public error = ''

  @Mutation
  public setLoading(state: boolean) {
    this.loading = state
  }

  @Mutation
  public editItem(item: Employee) {
    this.editedItem = Object.assign({}, item)
    this.editedIndex = this.items.indexOf(item)
  }

  @Mutation
  public cleanEditItem() {
    this.editedItem = Object.assign({}, this.defaultItem)
    this.editedIndex = -1
    this.error = ''
  }

  @Mutation
  public setData(items: Employee[]) {
    this.items = items
  }

  @Mutation
  public setError(msg: string) {
    this.error = msg
  }

  @Mutation
  public updateItem(payload: EmployeePayload) {
    if (payload.error) {
      this.error = payload.error
    } else {
      if (this.editedIndex === -1) {
        this.items.push(payload.item)
      } else {
        Object.assign(this.items[this.editedIndex], payload.item)
      }
    }
  }

  @Action
  public async getEmployee() {
    this.context.commit('setLoading', true)
    try {
      const { data } = await axios.get('/api/employee')
      this.context.commit('setData', data)
    } catch (error) {
      this.context.commit('setError', error.response.data.message)
    }
    this.context.commit('setLoading', false)
  }

  @Action
  public async deleteItem(id: string) {
    this.context.commit('setLoading', true)
    try {
      const { data } = await axios.delete(`/api/employee/${id}`)
      // TODO: remove employee from array
      // this.context.commit('setData', data)
    } catch (error) {
      this.context.commit('setError', error.response.data.message)
    }
    this.context.commit('setLoading', false)
  }

  @Action
  public async saveItem() {
    this.context.commit('setLoading', true)
    if (this.editedItem.id) {
      try {
        const { data } = await axios.put(
          `/api/employee/${this.editedItem.id.toString()}`,
          this.editedItem
        )
        this.context.commit('updateItem', { item: data, error: null })
      } catch (error) {
        this.context.commit('updateItem', {
          item: null,
          error: error.response.data.message
        })
        this.context.commit('setLoading', false)
      }
    } else {
      try {
        const { data } = await axios.post(`/api/employee`, this.editedItem)
        this.context.commit('updateItem', {
          item: data,
          error: null
        })
      } catch (error) {
        this.context.commit('updateItem', {
          item: null,
          error: error.response.data.message
        })
        this.context.commit('setLoading', false)
      }
    }
    this.context.commit('setLoading', false)
  }
}

export default getModule(EmployeeModule)
