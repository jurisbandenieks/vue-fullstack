export interface Employee {
  id?: number
  firstname: string
  lastname: string
  description: string
}

export interface EmployeePayload {
  item: Employee
  error: string
}
