export interface User {
  token: string
  user: {
    fullName: string
    role: string
    username: string
    email: string
  }
}

export interface UserLogin {
  username: string
  password: string
}

export interface UserRegister {
  fullName: string
  email: string
  username: string
  password: string
}
