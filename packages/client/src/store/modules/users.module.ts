import {
  Module,
  VuexModule,
  MutationAction,
  getModule,
  Mutation,
  Action
} from 'vuex-module-decorators'
import axios from 'axios'
import store from '@/store'
import { User, UserLogin, UserRegister } from './users.model'

@Module({
  namespaced: true,
  name: 'users',
  store,
  dynamic: true
})
class UsersModule extends VuexModule {
  public user: User | null = null
  public error: string | null = null

  get logged() {
    return this.user != null
  }

  get token() {
    if (this.user) {
      return this.user.token
    } else {
      return null
    }
  }

  @Mutation
  public setUser(user: User) {
    axios.defaults.headers.common.Authorization = `Bearer ${user.token}`
    this.user = user
  }

  @Mutation
  public setError(error: string) {
    this.error = error
  }

  @Mutation
  public logOut() {
    this.user = null
  }

  @MutationAction
  public async login(userLogin: UserLogin) {
    try {
      const { data } = await axios.post('/api/auth/local', userLogin)
      if (data && data.token) {
        axios.defaults.headers.common.Authorization = `Bearer ${data.token}`
        localStorage.setItem('bda-user', JSON.stringify(data))
        return { user: data as User, error: null }
      } else {
        return { user: {}, error: data.error }
      }
    } catch (error) {
      return { user: null, error: error.message }
    }
  }

  @Action
  public async register(userRegister: UserRegister) {
    try {
      await axios.post('/api/auth/local/register', userRegister)
    } catch (error) {
      this.setError(error.message)
    }
  }
}

export default getModule(UsersModule)
