import Vue from 'vue'
import Vuetify from 'vuetify'
import i18n from '../i18n'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#ee44aa',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  },
  lang: {
    t: (key, ...params) => i18n.t(key, params)
  }
})
